package dev.abhiroopsantra.advanced_linkedlist;

public class LinkedList2 {

  public static void main(String[] args) {
    // Let's pretend we are organizing daily tasks using Java's LinkedList.
    java.util.LinkedList<String> dailyTasks = new java.util.LinkedList<>();

    // Adding some tasks to our list.
    dailyTasks.add("Wake up");
    dailyTasks.add("Brush teeth");
    dailyTasks.add("Code Java");

    // Remove all elements from the dailyTasks linked list.
    dailyTasks.clear();

    // Print out if the list is empty now, expecting a true value.
    System.out.printf("Check if the list is empty? %s%n", dailyTasks.isEmpty());
  }
}
