package dev.abhiroopsantra.advanced_linkedlist;

import java.util.LinkedList;

public class LinkedList4 {

  public static void main(String[] args) {
    LinkedList<String> tasks = new LinkedList<>();
    tasks.add("Review tasks");
    tasks.add("Get coffee");
    tasks.add("Take a nap");
    // Add a task "Morning meeting" between "Get coffee" and "Take a nap"
    tasks.add(2, "Morning meeting");

    // Traverse the list with a for loop and print out each task
    for (String task : tasks) {
      System.out.println(task);
    }
  }
}
