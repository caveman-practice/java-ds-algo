package dev.abhiroopsantra.advanced_linkedlist;

public class LinkedList3 {

  public static void main(String[] args) {
    // Create a LinkedList to manage daily tasks.
    java.util.LinkedList<String> tasks = new java.util.LinkedList<>();
    tasks.add("Dinner break");

    // Set the first task of the day in your tasks list.
    tasks.addFirst("Check Facebook");
    // Set the last task of the day in your tasks list.
    tasks.addLast("Drink beer");

    // Check what tasks we have at the start and end of the day.
    System.out.println("Morning Task: " + tasks.getFirst());
    System.out.println("End of Day Task: " + tasks.getLast());
  }
}
