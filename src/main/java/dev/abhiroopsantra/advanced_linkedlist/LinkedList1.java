package dev.abhiroopsantra.advanced_linkedlist;

public class LinkedList1 {

  public static void main(String[] args) {
    java.util.LinkedList<String> tasks = new java.util.LinkedList<>();

    // Adding tasks for the day
    tasks.add("Email team meeting agenda");
    tasks.add("Review project proposal");
    // Add the task "Prepare presentation slides".
    tasks.add("Prepare presentation slides");

    // Remove the first task after completion.
    tasks.remove();

    // The remaining tasks in the list
    System.out.println(tasks);
  }
}
