package dev.abhiroopsantra.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

class Product {

  String name;
  double price;

  Product(String name, double price) {
    this.name = name;
    this.price = price;
  }

  public String getName() {
    return this.name;
  }

  public double getPrice() {
    return this.price;
  }

  public String toString() {
    return name + ": $" + price;
  }
}

public class Sorting2 {

  public static void main(String[] args) {
    ArrayList<Product> inventory = new ArrayList<>(
        Arrays.asList(
            new Product("Apple", 1.99),
            new Product("Milk", 2.99),
            new Product("Bread", 2.49)
        )
    );

    // Sort products in descending order by price.
    inventory.sort(Comparator.comparing(Product::getPrice).reversed());
    System.out.println(inventory);
  }
}
