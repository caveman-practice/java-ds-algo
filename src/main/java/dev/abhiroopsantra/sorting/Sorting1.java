package dev.abhiroopsantra.sorting;

import java.util.Arrays;

public class Sorting1 {

  public static void main(String[] args) {
    String[] inventory = {"Bananas", "Cherries", "Apples", "Dates"};

    // Sort the `inventory` array in alphabetical order and print it out.
    Arrays.sort(inventory);
    System.out.println(Arrays.toString(inventory));
  }
}
