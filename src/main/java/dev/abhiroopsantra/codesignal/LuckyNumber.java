package dev.abhiroopsantra.codesignal;

public class LuckyNumber {

  public static void main(String[] args) {
    System.out.println(solution(1230));
    System.out.println(solution(239017));
  }

  private static int[] splitNumber(int n) {
    String numberString = n + "";

    if (numberString.length() % 2 != 0) {
      System.out.println("The number does not have even number of digits");
      return null;
    }

    int mid = numberString.length() / 2;

    String firstHalfStr = numberString.substring(0, mid);
    String secondHalfStr = numberString.substring(mid);

    return new int[]{
        Integer.parseInt(firstHalfStr),
        Integer.parseInt(secondHalfStr)
    };
  }

  private static int sumOfDigits(int num) {
    if (num == 0) {
      return 0;
    }

    return num % 10 + sumOfDigits(num / 10);
  }

  static boolean solution(int n) {
    int[] splitNumbers = splitNumber(n);

    if (splitNumbers == null) {
      return false;
    }

    return sumOfDigits(splitNumbers[0]) == sumOfDigits(splitNumbers[1]);
  }
}
