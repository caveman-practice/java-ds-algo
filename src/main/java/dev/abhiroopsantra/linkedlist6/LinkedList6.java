package dev.abhiroopsantra.linkedlist6;

import dev.abhiroopsantra.linkedlist6.LinkedList.ListNode;
import java.util.Stack;

class LinkedList {

  ListNode head;

  public void append(int value) {
    if (head == null) {
      head = new ListNode(value);
    } else {
      ListNode current = head;
      while (current.next != null) {
        current = current.next;
      }
      current.next = new ListNode(value);
    }
  }

  static class ListNode {

    int value;
    ListNode next;

    ListNode(int value) {
      this.value = value;
      next = null;
    }
  }
}

public class LinkedList6 {

  public static void lastNElements(LinkedList.ListNode head, int n) {
    Stack<Integer> integerStack = new Stack<>();

    ListNode current = head;

    while (current.next != null) {
      integerStack.push(current.value);
      current = current.next;
    }
    integerStack.push(current.value);

    int popIndex = 0;
    while (popIndex < n) {
      System.out.print(integerStack.pop() + " ");
      popIndex++;
    }
  }

  public static void main(String[] args) {
    LinkedList list = new LinkedList();
    list.append(1);
    list.append(2);
    list.append(3);
    list.append(4);
    list.append(5);

    lastNElements(list.head, 3);

    // prints: 5 4 3
  }
}
