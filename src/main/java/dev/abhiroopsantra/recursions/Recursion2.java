package dev.abhiroopsantra.recursions;

public class Recursion2 {

  // Function to count the number of digits in a number using recursion
  static int countDigits(int num) {
    if (num == 0) {
      return 0;
    } else if (num % 2 == 0) {
      // Recursive case: reduce the number by dividing by 10 and count the rest
      return 1 + countDigits(num / 10);
    } else {
      return countDigits(num / 10);
    }
  }


  public static void main(String[] args) {
    System.out.println("The number of digits in the 'diameter of Mars': " + countDigits(7623));
  }
}
