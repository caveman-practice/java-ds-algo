package dev.abhiroopsantra;

import java.util.HashMap;

public class HashMapTest4 {

  public static void main(String[] args) {
    // Create a HashMap with String as the key type and String as the value type
    HashMap<String, String> books = new HashMap<>();

    // Add at least three books to the inventory with their ISBN and name
    books.put("1111", "The Rise of Nagash");
    books.put("2222", "The Fall of Altdorf");
    books.put("3333", "The Curse of Khaine");
    books.put("4444", "The Rise of the Horned Rat");
    books.put("5555", "The Lord of the End Times");

    // Display the entire bookstore inventory
    System.out.println("List of books: " + books);

    // Remove any book from the inventory
    books.remove("3333");

    // Display the entire bookstore inventory
    System.out.println("List of books: " + books);
  }
}
