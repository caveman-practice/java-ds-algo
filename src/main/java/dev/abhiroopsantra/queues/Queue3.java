package dev.abhiroopsantra.queues;

import java.util.LinkedList;
import java.util.Queue;

public class Queue3 {

  public static void main(String[] args) {
    // Initialize a new Queue of type String using LinkedList
    Queue<String> rideQueue = new LinkedList<>();
    // Add three visitors to the queue
    rideQueue.add("Captain Obvious");
    rideQueue.add("Calamity Jane");
    rideQueue.add("Citizen Kane");

    // Remove the visitor at the front of the queue and print who is on the ride
    System.out.println(rideQueue.remove() + " goes for a ride!!");

    // Peek at the next visitor in the queue and print who is next in line
    System.out.println(rideQueue.peek() + " is the next in line.");
  }
}
