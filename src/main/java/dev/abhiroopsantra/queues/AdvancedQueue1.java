package dev.abhiroopsantra.queues;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class AdvancedQueue1 {

  public static Queue<Integer> everyAlter(Queue<Integer> queue) {
    Queue<Integer> newQueue = new LinkedList<>();
    Queue<Integer> alterQueue = new LinkedList<>();
    int i = 0;

    // fill in new queue
    while (!queue.isEmpty()) {
      if (i % 2 == 0) {
        newQueue.add(queue.remove());
      } else {
        alterQueue.add(queue.remove());
      }
      i++;
    }

    return newQueue;
  }

  public static void main(String[] args) {

    // test case 1
    Queue<Integer> queue1 = new LinkedList<>();
    queue1.addAll(Arrays.asList(1, 2, 3, 4, 5, 6));
    queue1 = everyAlter(queue1);
    System.out.println(queue1); // Expected Output: [1, 3, 5]

    // test case 2
    Queue<Integer> queue2 = new LinkedList<>();
    queue2.addAll(Arrays.asList(1, 2));
    queue2 = everyAlter(queue2);
    System.out.println(queue2); // Expected Output: [1]

    // test case 3
    Queue<Integer> queue3 = new LinkedList<>();
    queue3.addAll(Arrays.asList(1, 2, 3, 4, 5, 6, 7));
    queue3 = everyAlter(queue3);
    System.out.println(queue3); // Expected Output: [1, 3, 5, 7]
  }
}
