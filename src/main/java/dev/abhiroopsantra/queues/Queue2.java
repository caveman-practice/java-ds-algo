package dev.abhiroopsantra.queues;


public class Queue2 {

  public static void main(String[] args) {
    QueueTwo queue = new QueueTwo(5);
    queue.enqueue(1);
    queue.enqueue(2);
    // Use dequeue method to remove and display the visitor taking the ride
    System.out.println(queue.dequeue() + " goes for a ride!!");
  }

  static class QueueTwo {

    int front, rear, capacity;
    int[] elements;

    public QueueTwo(int capacity) {
      this.capacity = capacity;
      elements = new int[this.capacity];
      front = 0;
      rear = 0;
    }

    // Adds an element to the rear of the queue
    public void enqueue(int element) {
      // Check if queue is not full
      if (rear == capacity) {
        return;
      }
      // If not full, add element to the queue and adjust the rear
      elements[rear] = element;
      rear = (rear + 1) % capacity;
    }

    // Removes and returns an element from the front of the queue
    public int dequeue() {
      // Check if queue is not empty
      if (front == rear) {
        return -1;
      }
      // If not empty, return the front element and move the front forward
      int element = elements[front];
      front = (front + 1) % capacity;
      return element;
    }
  }
}
