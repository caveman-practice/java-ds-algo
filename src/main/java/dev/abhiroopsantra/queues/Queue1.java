package dev.abhiroopsantra.queues;

class QueueOne {

  int front, rear, size, capacity;
  int[] array;

  public QueueOne(int capacity) {
    this.capacity = capacity;  // Set max size
    front = size = 0;  // Initialize front and size
    rear = capacity - 1;  // Initialize rear
    array = new int[this.capacity];
  }

  boolean isFull(QueueOne queue) {
    return (queue.size == queue.capacity);
  }

  boolean isEmpty(QueueOne queue) {
    return (queue.size == 0);
  }

  void enqueue(int item) {
    if (isFull(this)) {
      return;  // Check if full
    }
    rear = (rear + 1) % capacity;
    array[rear] = item;
    size++;
  }

  int dequeue() {
    if (isEmpty(this)) {
      return Integer.MIN_VALUE;  // Check if empty
    }
    int item = array[front];
    front = (front + 1) % capacity;
    size--;
    return item;
  }
}

public class Queue1 {

  public static void main(String[] args) {
    QueueOne queue = new QueueOne(3);  // Theme park queue for 3 rides
    queue.enqueue(1);  // First person joins the queue
    queue.enqueue(2);  // Second person joins the queue
    System.out.println(queue.dequeue() + " goes for the ride!");  // First person goes for the ride
  }
}
