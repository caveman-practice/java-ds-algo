package dev.abhiroopsantra;

import java.util.HashSet;

public class Main {

  public static void main(String[] args) {

    // Instantiate a collection to store unique visitor IDs
    HashSet<String> uniqueVisitors = new HashSet<>();

    // Add two different visitor IDs to your collection and try adding one of them again
    uniqueVisitors.add("abhiroop.santra@gmail.com");
    uniqueVisitors.add("arun.santra@yahoo.com");
    uniqueVisitors.add("abhiroop.santra@gmail.com");

    // Display the number of unique visitors
    System.out.println(uniqueVisitors.size());
  }
}