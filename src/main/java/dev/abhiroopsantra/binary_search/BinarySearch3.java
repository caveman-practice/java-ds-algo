package dev.abhiroopsantra.binary_search;

public class BinarySearch3 {

  static int findBookIndex(int[] ids, int start, int end, int bookId) {
    if (start > end) {
      return -1; // Base case: Book not found
    }
    int mid = start + (end - start) / 2; // Find the midpoint
    if (ids[mid] == bookId) {
      return mid; // Book found
    }
    if (ids[mid] > bookId) {
      return findBookIndex(ids, start, mid - 1, bookId); // Check the left half
    }
    return findBookIndex(ids, mid + 1, end, bookId); // Check the right half
  }

  public static void main(String[] args) {
    int[] bookIds = {2, 4, 6, 8, 10, 12}; // Sorted array of books by IDs
    int bookToFind = 2; // ID of the book to find
    System.out.println(findBookIndex(bookIds, 0, bookIds.length - 1, bookToFind)); // Outputs: 0
  }
}
