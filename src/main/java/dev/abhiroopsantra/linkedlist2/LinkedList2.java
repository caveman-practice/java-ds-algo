package dev.abhiroopsantra.linkedlist2;

class Node {

  int data;
  Node next;

  Node(int d) {
    data = d;
    next = null;
  }
}

class RailwayNetwork {

  Node head;

  // Method to add a new station to the network
  void addStation(int stationData) {
    Node newNode = new Node(stationData);

    if (head == null) {
      head = newNode;
      return;
    }

    newNode.next = head;
    head = newNode;
  }

  void show() {
    Node current = head;
    while (current.next != null) {
      System.out.print(current.data + "-");
      current = current.next;
    }
    System.out.print(current.data);
  }

}

public class LinkedList2 {

  public static void main(String[] args) {
    RailwayNetwork network = new RailwayNetwork();
    // Adding stations with their station IDs
    network.addStation(101);
    network.addStation(202);
    network.addStation(303);
    network.show();
  }
}
