package dev.abhiroopsantra.linkedlist3;

class Node {

  int data;
  Node next;

  Node(int d) {
    data = d;
    next = null;
  }
}

class RailwayNetwork {

  Node head;

  // Method to add a new station to the network
  void addStation(int stationData) {
    Node newStation = new Node(stationData);
    // check if head is null. If it is, initialize head with newStation
    if (head == null) {
      head = newStation;
      return;
    }

    // else append the newStation to the end of the list
    Node last = head;
    while (last.next != null) {
      last = last.next;
    }

    last.next = newStation;
  }

  void show() {
    Node current = head;
    while (current.next != null) {
      System.out.print(current.data + "-");
      current = current.next;
    }
    System.out.print(current.data);
  }

}

public class LinkedList3 {

  public static void main(String[] args) {
    RailwayNetwork network = new RailwayNetwork();
    // Adding stations with their station IDs
    network.addStation(101);
    network.addStation(202);
    network.addStation(303);
    network.show();
  }
}
