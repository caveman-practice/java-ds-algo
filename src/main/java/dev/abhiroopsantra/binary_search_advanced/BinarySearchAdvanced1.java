package dev.abhiroopsantra.binary_search_advanced;

public class BinarySearchAdvanced1 {

  public static int findPosition(int[] arr, int x) {
    int peak = findPeak(arr);

    int searchResult = binarySearch(arr, x, 0, peak, true);

    if (searchResult != -1) {
      return searchResult;
    } else {
      return binarySearch(arr, x, peak + 1, arr.length - 1, false);
    }
  }

  static int findPeak(int[] arr) {
    int low = 0, high = arr.length - 1;
    while (low < high) {
      int mid = low + (high - low) / 2;

      if (arr[mid] > arr[mid + 1]) {
        high = mid;
      } else {
        low = mid + 1;
      }
    }

    return low;
  }

  static int binarySearch(int[] arr, int x, int low, int high, boolean ascending) {
    while (low <= high) {
      int mid = low + (high - low) / 2;
      if (arr[mid] == x) {
        return mid;
      } else if (ascending) {
        if (arr[mid] < x) {
          low = mid + 1;
        } else {
          high = mid - 1;
        }
      } else {
        if (arr[mid] > x) {
          low = mid + 1;
        } else {
          high = mid - 1;
        }
      }
    }
    return -1;
  }

  public static void main(String[] args) {
    int[] arr = {-3, -2, 4, 6, 10, 8, 7, 1};
    int x = 7;
    int position = findPosition(arr, x);
    if (position == -1) {
      System.out.println("Element Not Present");
    } else {
      System.out.println("Element Present at Index " + position);
    }
  }
}
