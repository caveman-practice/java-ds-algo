package dev.abhiroopsantra;

import java.util.HashMap;

public class HashMapTest2 {

  public static void main(String[] args) {
    // Create a HashMap to track the quantity of books by their unique IDs
    HashMap<Integer, Integer> bookInventory = new HashMap<>();

    // Adding books with their unique IDs as keys and quantities as values
    bookInventory.put(1001, 5); // ID 1001 has 5 copies
    bookInventory.put(1002, 3); // ID 1002 has 3 copies

    // Remove the book with ID 1001 from the inventory
    int removedValue = bookInventory.remove(1001);
    System.out.println(removedValue);

    // Print the updated inventory
    System.out.println("Updated book inventory: " + bookInventory);
  }
}
