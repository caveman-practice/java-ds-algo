package dev.abhiroopsantra.quick_sort;

public class QuickSort2 {

  static int partitionCelestial(int[] magnitudes, int start, int end) {
    int pivot = magnitudes[end];
    int i = start - 1;
    for (int j = start; j < end; j++) {
      if (magnitudes[j] <= pivot) {
        i++;
      }
    }

    int temp = magnitudes[i + 1];
    magnitudes[i + 1] = magnitudes[end];
    magnitudes[end] = temp;

    return i + 1;
  }

  public static void main(String[] args) {
    int[] celestialMagnitudes = {5, 3, 0, -1, -2, 1};
    int pivotPosition = partitionCelestial(celestialMagnitudes, 0, celestialMagnitudes.length - 1);
    // Now, celestialMagnitudes is partitioned, with pivot in the correct position.
    System.out.println(pivotPosition);
  }
}
