package dev.abhiroopsantra.linkedlist7;

class ListNode {

  int value;
  ListNode next;

  ListNode(int value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {

  ListNode head;

  public void append(int value) {
    if (head == null) {
      head = new ListNode(value);
      return;
    }
    ListNode current = head;
    while (current.next != null) {
      current = current.next;
    }
    current.next = new ListNode(value);
  }
}

public class LinkedList7 {

  public static int indexOfX(LinkedList list, int x) {
    ListNode current = list.head;
    int position = 0;

    if (current.value == x) {
      return position;
    }

    while (current.next != null) {
      position++;
      current = current.next;

      if (current.value == x) {
        return position;
      }
    }

    return -1;
  }

  public static void main(String[] args) {
    LinkedList list = new LinkedList();
    list.append(5);
    list.append(10);
    list.append(15);
    int index = indexOfX(list, 100);
    System.out.println("The value 10 first occurs at position " + index);
  }
}
