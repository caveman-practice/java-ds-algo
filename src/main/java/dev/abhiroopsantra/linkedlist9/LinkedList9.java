package dev.abhiroopsantra.linkedlist9;

class ListNode {

  int value;
  ListNode next;

  ListNode(int value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {

  ListNode head;

  public void append(int value) {
    if (head == null) {
      head = new ListNode(value);
      return;
    }
    ListNode current = head;
    while (current.next != null) {
      current = current.next;
    }
    current.next = new ListNode(value);
  }
}

public class LinkedList9 {

  public static int sumOfEverySecond(LinkedList list) {
    if (list == null || list.head == null || list.head.next == null) {
      return 0;
    }

    int sum = 0;
    int index = 0;
    ListNode current = list.head;

    while (current.next != null) {
      index++;
      if (index % 2 == 1) {
        sum += current.value;
      }
      current = current.next;
    }

    sum += current.value;
    return sum;
  }

  public static void main(String[] args) {
    LinkedList list = new LinkedList();
    list.append(5);
    int sum = sumOfEverySecond(list);
    System.out.println("Sum of Every Second Node Value: " + sum);
    list.append(10);
    list.append(15);
    sum = sumOfEverySecond(list);
    System.out.println("Sum of Every Second Node Value: " + sum);
  }
}
