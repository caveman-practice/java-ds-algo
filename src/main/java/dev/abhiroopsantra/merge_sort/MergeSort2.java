package dev.abhiroopsantra.merge_sort;

public class MergeSort2 {

  static void mergeSort(int[] arr, int left, int right) {
    if (left < right) {
      int mid = (left + right) / 2;
      mergeSort(arr, left, mid);
      mergeSort(arr, mid + 1, right);
      merge(arr, left, mid, right);
    }
  }

  static void merge(int[] arr, int left, int mid, int right) {
    int n1 = mid - left + 1;
    int n2 = right - mid;
    int[] leftArr = new int[n1];
    int[] rightArr = new int[n2];

    System.arraycopy(arr, left, leftArr, 0, n1);
    for (int j = 0; j < n2; ++j) {
      rightArr[j] = arr[mid + 1 + j];
    }

    int i = 0, j = 0;
    int k = left;
    while (i < n1 && j < n2) {
      // the bug should be somewhere here
      if (leftArr[i] <= rightArr[j]) {
        arr[k] = leftArr[i];
        i++;
      } else {
        arr[k] = rightArr[j];
        j++;
      }
      k++;
    }

    while (i < n1) {
      arr[k] = leftArr[i];
      i++;
      k++;
    }

    while (j < n2) {
      arr[k] = rightArr[j];
      j++;
      k++;
    }
  }

  public static void main(String[] args) {
    int[] musicLibrary = {3, 5, 1, 2, 6, 9, 8, 8, 3};
    mergeSort(musicLibrary, 0, musicLibrary.length - 1);

    for (int songID : musicLibrary) {
      System.out.print(songID + " ");
    }
  }
}
