package dev.abhiroopsantra;

import java.util.Arrays;
import java.util.HashSet;

public class DuplicateArrays {

  public static void main(String[] args) {
    int[] addresses = {1, 2, 3, 2, 1, 5, 3, 1, 2, 1, 4, 5, 6};
    int[] uniqueAddresses = processAddresses(addresses);
    System.out.println(Arrays.toString(uniqueAddresses));   // Returns [1, 2, 3, 5, 4, 6]
  }

  static int[] processAddresses(int[] addresses) {
    HashSet<Integer> set = new HashSet<>();

    if (addresses.length == 0) {
      return new int[0];
    }

    for (int i : addresses) {
      set.add(i);
    }

    int[] result = new int[set.size()];

    int index = 0;
    for (int num : set) {
      result[index++] = num;
    }

    return result;
  }
}
