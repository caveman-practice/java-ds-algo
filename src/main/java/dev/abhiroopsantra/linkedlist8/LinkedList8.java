package dev.abhiroopsantra.linkedlist8;

import java.util.HashSet;

class ListNode {

  int value;
  ListNode next;

  ListNode(int value) {
    this.value = value;
    this.next = null;
  }
}

class LinkedList {

  private final ListNode head;

  public LinkedList(int value) {
    this.head = new ListNode(value);
  }

  public void append(int value) {
    ListNode newNode = new ListNode(value);
    ListNode current = head;

    if (current.next == null) {
      current.next = newNode;
    } else {
      while (current.next != null) {
        current = current.next;
      }
      current.next = newNode;
    }
  }

  public void removeDuplicates() {
    HashSet<Integer> seenBooks = new HashSet<>();

    ListNode current = head;
    seenBooks.add(current.value);

    while (current.next != null) {
      if (seenBooks.contains(current.next.value)) {
        current.next = current.next.next;
      } else {
        seenBooks.add(current.next.value);
        current = current.next;
      }
    }
  }

  public void printList() {
    ListNode current = head;
    while (current != null) {
      System.out.println(current.value);
      current = current.next;
    }
  }
}

public class LinkedList8 {

  public static void main(String[] args) {
    LinkedList linkedList = new LinkedList(1);
    linkedList.append(2);
    linkedList.append(2);
    linkedList.append(3);
    linkedList.append(3);
    linkedList.append(3);

    linkedList.removeDuplicates();
    linkedList.printList();
  }
}
