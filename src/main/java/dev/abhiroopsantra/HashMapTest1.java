package dev.abhiroopsantra;

import java.util.HashMap;

public class HashMapTest1 {

  public static void main(String[] args) {
    // HashMap to store books (ISBN) and their average reviews
    HashMap<String, Double> bookReviews = new HashMap<>();

    // Adding some books and their reviews to the inventory
    bookReviews.put("978-0134685991", 4.7); // Effective Java
    bookReviews.put("978-0596009205", 4.5); // Head First Java

    // Printing the average review of "Effective Java"
    System.out.println("Average review for Effective Java: " + bookReviews.get("978-0134685991"));
  }
}
