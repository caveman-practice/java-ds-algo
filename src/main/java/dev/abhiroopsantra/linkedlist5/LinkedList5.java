package dev.abhiroopsantra.linkedlist5;

class Node {

  int data;
  Node next;
  // introduce station names
  String stationName;

  Node(int d, String sn) {
    data = d;
    next = null;
    stationName = sn;
  }
}

class RailwayNetwork {

  Node head;

  // Add station name here
  void addStation(int stationData, String stationName) {
    Node newStation = new Node(stationData, stationName);
    if (head == null) {
      head = newStation;
    } else {
      Node last = head;
      while (last.next != null) {
        last = last.next;
      }
      last.next = newStation;
    }
  }

  // modify the show method to include station names
  void show() {
    Node current = head;
    while (current.next != null) {
      System.out.print(current.stationName + " ----> ");
      current = current.next;
    }
    System.out.println(current.stationName);
  }

}

public class LinkedList5 {

  public static void main(String[] args) {
    RailwayNetwork network = new RailwayNetwork();
    // Add station names here
    network.addStation(101, "Station1");
    network.addStation(202, "Station2");
    network.addStation(303, "Station3");
    network.show();
  }
}
