package dev.abhiroopsantra.advanced_sorting;

public class KthOrdinal {

  public static int findKthLargest(int[] numbers, int k) {
    if (numbers == null || numbers.length < k) {
      return Integer.MIN_VALUE;
    }
    return kthLargest(numbers, 0, numbers.length - 1, k);
  }

  static int kthLargest(int[] arr, int start, int end, int k) {
    if (k > 0 && k <= end - start + 1) {
      // use partition to find pivot's position
      int pivotIndex = partition(arr, start, end);

      // if pivot is in correct position, return the answer
      if (pivotIndex - start == k - 1) {
        return arr[pivotIndex];
      }

      // call kthLargest recursively in case position is greater than k
      if (pivotIndex - start > k - 1) {
        return kthLargest(arr, start, pivotIndex - 1, k);
      }

      // call kthLargest recursively in case position is greater than k
      return kthLargest(arr, pivotIndex + 1, end, k - pivotIndex + start - 1);

    }

    return Integer.MAX_VALUE;
  }

  static int partition(int[] arr, int start, int end) {
    int pivot = arr[start];
    int i = start;

    for (int j = start + 1; j <= end; j++) {
      if (arr[j] >= pivot) {
        i++;
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
      }
    }

    int temp = arr[i];
    arr[i] = arr[start];
    arr[start] = temp;

    return i;
  }

  public static void main(String[] args) {
    int[] scores = {1, 7, 2, 4, 2, 1, 6};
    System.out.println(findKthLargest(scores, 3));  // It should print 4
  }
}
