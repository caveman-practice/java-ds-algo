package dev.abhiroopsantra.stacks;

import java.util.EmptyStackException;
import java.util.Stack;

public class Stack5 {

  public static void main(String[] args) {
    // TODO: Create a Stack object to simulate a stack of plates
    Stack<Integer> stackOfPlates = new Stack<>();

    // TODO: Add three plates to the stack (numbered 1, 2, 3)
    stackOfPlates.push(1);
    stackOfPlates.push(2);
    stackOfPlates.push(3);

    // TODO: Remove all the plates from the stack. Print initial stack state and its state after each removal
    for (int i = 0; i < 3; i++) {
      System.out.println("Current Stack State: " + stackOfPlates);
      try {
        stackOfPlates.pop();
      } catch (EmptyStackException ex) {
        System.out.println("Stack is empty");
      }
    }
  }
}
