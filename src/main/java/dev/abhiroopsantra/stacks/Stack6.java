package dev.abhiroopsantra.stacks;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Stack;

public class Stack6 {

  public static void main(String[] args) {
    System.out.println(areParenthesesBalanced("(())()"));  // prints: true
    System.out.println(areParenthesesBalanced("())("));  // prints: false
    System.out.println(areParenthesesBalanced("("));  // prints: false
  }

  public static boolean areParenthesesBalanced(String inputStr) {
    HashMap<Character, Character> bracketPairs = new HashMap<>();
    bracketPairs.put('(', ')');
    bracketPairs.put('{', '}');
    bracketPairs.put('[', ']');

    HashSet<Character> openBrackets = new HashSet<>();
    openBrackets.add('(');
    openBrackets.add('{');
    openBrackets.add('[');

    Stack<Character> brackets = new Stack<>();

    for (int i = 0; i < inputStr.length(); i++) {
      char currentChar = inputStr.charAt(i);

      if (openBrackets.contains(currentChar)) {
        brackets.push(currentChar);
      } else if (!brackets.isEmpty() && currentChar == bracketPairs.get(brackets.peek())) {
        brackets.pop();
      } else {
        return false;
      }
    }

    return brackets.isEmpty();
  }
}
