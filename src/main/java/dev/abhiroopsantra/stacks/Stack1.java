package dev.abhiroopsantra.stacks;

class Stack {

  private final int[] dishesStack;
  private int top = -1;

  public Stack(int capacity) {
    dishesStack = new int[capacity];
  }

  public void push(int dish) {
    if (top < dishesStack.length - 1) {
      dishesStack[++top] = dish;
    } else {
      System.out.println("Kitchen shelf is full!");
    }
  }

  public int pop() {
    if (top > -1) {
      return dishesStack[top--];
    } else {
      System.out.println("No dishes to process!");
      return -1;
    }
  }

  public int peek() {
    if (top > -1) {
      return dishesStack[top];
    } else {
      System.out.println("Kitchen shelf is empty!");
      return -1;
    }
  }
}

public class Stack1 {

  public static void main(String[] args) {
    Stack kitchenStack = new Stack(3); // A stack for holding dishes
    kitchenStack.push(1); // Plate 1 arrives
    kitchenStack.push(2); // Plate 2 arrives

    System.out.println(kitchenStack.peek()); // Check the top plate (should be 2)

    kitchenStack.pop(); // Process Plate 2
    kitchenStack.pop(); // Process Plate 1
    kitchenStack.pop(); // Try to process a non-existent plate
  }
}
