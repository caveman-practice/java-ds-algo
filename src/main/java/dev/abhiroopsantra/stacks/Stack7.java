package dev.abhiroopsantra.stacks;

import java.util.Stack;

public class Stack7 {

  public static void main(String[] args) {
    System.out.println(reverseAndFlipCase("Hello, World!")); // Expected Output: "!DLROw ,OLLEh"
    System.out.println(reverseAndFlipCase("hello")); // Expected Output: "OLLEH"
    System.out.println(reverseAndFlipCase("HELLO")); // Expected Output: "olleh"
  }

  public static String reverseAndFlipCase(String str) {
    Stack<Character> stack = new Stack<>();

    char[] charArray = str.toCharArray();
    for (char ch : charArray) {
      if (Character.isUpperCase(ch)) {
        stack.push(Character.toLowerCase(ch));
      } else {
        stack.push(Character.toUpperCase(ch));
      }
    }

    StringBuilder reversed = new StringBuilder();
    while (!stack.isEmpty()) {
      reversed.append(stack.pop());
    }

    return reversed.toString();
  }
}
