package dev.abhiroopsantra.stacks;

class StackTwo {

  private final int maxSize;
  private final int[] dishStack;
  private int top;

  public StackTwo(int s) {
    maxSize = s;
    dishStack = new int[maxSize];
    top = -1;
  }

  // TODO: Add the method to push a new dish onto the stack
  public void push(int element) {
    if (top < maxSize - 1) {
      dishStack[++top] = element;
    } else {
      System.out.println("You can't add any more dishes");
    }
  }
  // If the stack is full, let the chef know they can't add more dishes!
}

public class Stack2 {

  public static void main(String[] args) {
    Stack plateStack = new Stack(3); // A stack to hold 3 plates
    plateStack.push(1); // Add a plate with ID 1
    plateStack.push(2); // Add a plate with ID 2
    plateStack.push(3); // Add a plate with ID 3
    plateStack.push(4); // Fail to add a plate with ID 4 as stack is full
  }
}
