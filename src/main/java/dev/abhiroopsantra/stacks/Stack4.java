package dev.abhiroopsantra.stacks;

import java.util.Stack;

public class Stack4 {

  public static void main(String[] args) {
    // Create a stack to represent plates
    Stack<String> platesStack = new Stack<>();

    // Add three plates to the stack
    platesStack.push("Plate 1");
    platesStack.push("Plate 2");
    platesStack.push("Plate 3");

    // Wash the topmost plate
    String washedPlate = platesStack.pop();
    System.out.println("Washed: " + washedPlate);
  }
}
