package dev.abhiroopsantra.stacks;

class StackThree {

  private final int capacity;
  private final int[] kitchenOrders;
  private int topIndex = -1;

  public StackThree(int capacity) {
    this.capacity = capacity;
    this.kitchenOrders = new int[capacity];
  }

  void addOrder(int order) {
    if (topIndex < capacity - 1) {
      kitchenOrders[++topIndex] = order;
    } else {
      System.out.println("Kitchen is too busy, cannot take more orders!");
    }
  }

  int nextOrder() {
    if (topIndex > -1) {
      return kitchenOrders[topIndex--];
    } else {
      System.out.println("No orders to prepare!");
      return -1;
    }
  }
}

public class Stack3 {

  public static void main(String[] args) {
    StackThree kitchenStack = new StackThree(5);
    kitchenStack.addOrder(101); // Order #101 added
    kitchenStack.addOrder(102); // Order #102 added
    kitchenStack.addOrder(103); // Order #103 added
    kitchenStack.addOrder(104); // Order #104 added
    kitchenStack.addOrder(105); // Order #105 added
    System.out.println(
        "Next order to prepare: " + kitchenStack.nextOrder()); // Should print order #105
  }
}
