package dev.abhiroopsantra;

import java.util.HashMap;

public class HashMapTest3 {

  public static void main(String[] args) {
    // Creating a HashMap for the Online Bookstore Inventory
    HashMap<String, Integer> books = new HashMap<>();

    // Adding key-value pairs for BookName-Quantity
    books.put("The Alchemist", 10);
    // Add any other books to the bookstore inventory along with their quantity
    books.put("Good Vibes, Good Life", 5);
    books.put("Horus Rising", 9);
    books.put("Lord of the End Times", 8);

    // Display the quantity of "The Alchemist"
    System.out.println(books.get("The Alchemist"));
  }
}
